import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import Spreadsheet from "x-data-spreadsheet";

@Component({
  selector: 'app-excel-editor',
  templateUrl: './excel-editor.component.html',
  styleUrls: ['./excel-editor.component.css']
})
export class ExcelEditorComponent implements OnInit {

  constructor() { }

  @ViewChild('xspreadsheet',{static: true}) spreadSheet: any;
  @Input('data') data: string;
  @Output('DataChange') dataChange = new EventEmitter();

  change: number = 0;

  ngOnInit() {
    const config = {
      showToolbar: true,
      showGrid: true,
      showContextmenu: true,
      view: {
        height: () => this.spreadSheet.nativeElement.clientHeight,
        width: () => this.spreadSheet.nativeElement.clientWidth,
      },
      row: {
        len: 100,
        height: 25,
      },
      col: {
        len: 26,
        width: 100,
        indexWidth: 60,
        minWidth: 60,
      },
      style: {
        bgcolor: '#ffffff',
        align: 'left',
        valign: 'middle',
        textwrap: false,
        strike: false,
        underline: false,
        color: '#0a0a0a',
        font: {
          name: 'Helvetica',
          size: 10,
          bold: false,
          italic: false,
        },
      },
    };
    const s = new Spreadsheet(this.spreadSheet.nativeElement, config)
      .loadData(this.data) // load data
      .change(data => {
       // this.dataChange.emit();
        this.change++;
      });
    
    s.validate();
  }

  

}
